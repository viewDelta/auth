package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"time"

	_ "bitbucket.org/viewDelta/auth/docs"
	"bitbucket.org/viewDelta/auth/handler"
	"bitbucket.org/viewDelta/service-core/config"
	endpoints "bitbucket.org/viewDelta/service-core/constants"
	settings "bitbucket.org/viewDelta/service-core/constants"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	echoSwagger "github.com/swaggo/echo-swagger"
)

// @title RetailAuth
// @description This is auth service for authentication.
// @version 2.0
// @contact.name Abinash Pattajoshi
// @host localhost:8082
// @BasePath /auth
// @tag.name auth
// @tag.description Authorization module
// @schemes http

func main() {
	//Read Property Loader
	prop := config.GetConfiguration()

	//Echo Router
	router := echo.New()
	router.Use(
		middleware.Secure(),
		middleware.Recover(),
		middleware.RequestID(),
	)
	router.Debug = true
	router.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		//AllowOrigins:     []string{config.GetEnv("ORIGIN")},
		AllowCredentials: true,
		AllowHeaders:     []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, settings.TOKEN_HEADER, settings.X_Forwarded_Host},
		AllowMethods:     []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
	router.Logger.SetLevel(log.INFO)
	router.HTTPErrorHandler = func(err error, c echo.Context) {
		// Take required information from error and context and send it to a service like New Relic
		log.Error(c.Path(), c.QueryParams(), err.Error())
		// Call the default handler to return the HTTP response
		router.DefaultHTTPErrorHandler(err, c)
	}
	group := router.Group("/auth")
	{
		authHandler := handler.New()
		group.POST(endpoints.Login, authHandler.Login)
		group.POST(endpoints.SignUp, authHandler.SignUp)
		group.PUT(endpoints.VerifyAccount, authHandler.VerifyAccount)
		group.PUT(endpoints.RestorePassword, authHandler.ResetPassword)
		group.GET(endpoints.FetchNonAdmin, authHandler.FetchNonAdminUsers)
		group.PUT(endpoints.UserDirective, authHandler.ActivateUser)
		group.DELETE(endpoints.UserDirective, authHandler.RemoveUser)
		group.PUT(endpoints.UpdateUsers, authHandler.UpdateUserRole)
	}
	router.GET("/swagger/*", echoSwagger.WrapHandler)

	log.Infof("%v-%v Started on --> %v", prop.Application.Name, prop.Application.Version, prop.Application.Port)
	// Start server
	go func() {
		if err := router.Start(":" + prop.Application.Port); err != nil {
			log.Infof("shutting down the server %v", err)
		}
	}()
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	//Closing the log file upon receiving the signal
	defer func() {
		if err := config.CloseRedisClient(); err != nil {
			log.Errorf("Redis Connection Close error [%v]", err)
		}
		config.CloseLogFile()
	}()
	if err := router.Shutdown(ctx); err != nil {
		router.Logger.Fatal(err)
	}
}
