#Simple Docker Image
FROM alpine:3.14
LABEL Author="Abinash <apattajoshi@outlook.com>" 
EXPOSE 8082 

#Work Location Image
ARG APP_HOME="/usr/local/authService/" 
ARG LOG_DIR="/var/retailGo/logs/"
ARG APP="bin/*"
ARG SWAGGER="docs/*"

RUN mkdir -p ${APP_HOME}/docs
COPY ${SWAGGER} ${APP_HOME}/docs
RUN mkdir -p ${APP_HOME}
RUN mkdir -p ${LOG_DIR}
RUN chmod 777 -R ${LOG_DIR}
#Log File Directory Exposed which will be mapped during containerization
VOLUME [ "${LOG_DIR}" ]

COPY ${APP} ${APP_HOME} 
WORKDIR ${APP_HOME}
CMD ["./auth"]