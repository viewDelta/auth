.PHONY: clean
clean:
	rm -rf bin

.PHONY: build
build: clean
	#Ubuntu
	#env GOOS=linux GOARCH=amd64 go build -o bin/auth
	#Alpine:3.11
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bin/auth
	cp *.yml bin/
	cp .env bin/
	chmod 0444 bin/auth
	chmod +x bin/auth

.PHONY: image
image: build
	docker build -t auth:1.0 .
