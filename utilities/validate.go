package utilities

import (
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/viewDelta/service-core/constants"
	Message "bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/intercept"
	"bitbucket.org/viewDelta/service-core/locales/request"
	"bitbucket.org/viewDelta/service-core/locales/response"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/utils"
	"github.com/labstack/gommon/log"
	"github.com/rs/xid"
	"go.mongodb.org/mongo-driver/mongo"
)

//IAuthValidator abstraction over validate functions
type IAuthValidator interface {
	ValidateSignUpReqPayload(reqBody *request.SignUpReq) error
	ValidateSignInReqPayload(reqBody *request.LoginReq) (*models.Users, error)
	ValidatePassword(password string) error
	ValidateAndFormUpdateRoleUserObject(reqBody []request.UpdateRolesRequest) ([]models.Users, []response.Details)
}

type authValidator struct {
	iValidator utils.Ivalidator
}

var v *authValidator

//GetValidator - will return validate interface reference
func GetValidator() IAuthValidator {
	if v == nil {
		if validator, err := utils.NewValidatorInstance(true); err != nil {
			log.Errorf("Error While Getting Core-Validator Instance - [%v]", err)
			return nil
		} else {
			v = &authValidator{
				iValidator: validator,
			}
		}
	}
	return v
}

//ValidateSignUpReqPayload will validate the signup request payload
func (v *authValidator) ValidateSignUpReqPayload(reqBody *request.SignUpReq) error {
	//Name Validation
	if v.iValidator.ValidateName(reqBody.FirstName) && v.iValidator.ValidateName(reqBody.LastName) {
		//Email Validation
		if len(strings.TrimSpace(reqBody.Email)) < 1 {
			return fmt.Errorf(constants.EMAIL_IS_REQUIRED)
		}
		if user, _ := v.iValidator.ValidateEmailExsits(reqBody.Email); user != nil {
			return fmt.Errorf(Message.EMAIL_ALREADY_EXISTS)
		}
		//Password Validation
		if err := v.iValidator.ValidatePwd(reqBody.Password); err != nil {
			return err
		}
		//Validate RoleID
		if err := v.iValidator.ValidateRoleID(reqBody.RoleID); err != nil {
			return err
		}
	} else {
		return fmt.Errorf(Message.INVALID_NAME)
	}
	return nil
}

//ValidateSignInReqPayload will verify whether login cred are valid or not
func (v *authValidator) ValidateSignInReqPayload(reqBody *request.LoginReq) (*models.Users, error) {
	if user, _ := v.iValidator.ValidateEmailExsits(reqBody.Email); user != nil {
		if CheckPasswordHash(reqBody.Password, user.Password) {
			return user, nil
		}
		return nil, fmt.Errorf(Message.INVALID_CREDENTIALS)
	}
	return nil, fmt.Errorf(Message.INVALID_EMAIL)
}

//ValidatePassword will verify the password request
func (v *authValidator) ValidatePassword(password string) error {
	return v.iValidator.ValidatePwd(password)
}

// ValidateAndFormUpdateRoleUserObject will validate the Update User Roles
func (v *authValidator) ValidateAndFormUpdateRoleUserObject(reqBody []request.UpdateRolesRequest) (userList []models.Users, errList []response.Details) {
	log.Info("Entering validate.ValidateAndFormUpdateRoleUserObject() Method")
	xid := xid.New()
	for _, updateReq := range reqBody {
		if checkIfExistsInList(userList, updateReq.UID) {
			log.Errorf("Role update request duplication for UID - %v", updateReq.UID)
			errList = append(errList, response.NewErrorDetail(http.StatusAlreadyReported, fmt.Sprintf("Duplication UID update received - [%v]", updateReq.UID)))
		} else if err := xid.UnmarshalText([]byte(updateReq.UID)); err != nil { // Format validation
			log.Errorf("Invalid format of UID received - %v", updateReq.UID)
			errList = append(errList, response.NewErrorDetail(http.StatusBadRequest, fmt.Sprintf("Invalid UID received - [%v]", updateReq.UID)))
		} else if err := v.iValidator.ValidateRoleID(updateReq.ROLES); err != nil { //Validate RoleID
			log.Errorf("Invalid RoleID content found for UID - %v, Requested Roles - %v", updateReq.UID, updateReq.ROLES)
			errList = append(errList, response.NewErrorDetail(http.StatusBadRequest, fmt.Sprintf("UID - [%v] Update error - [%v]", updateReq.UID, err)))
		} else if user, err := intercept.NonAdminValidator(updateReq.UID); err != nil { //Db uid validation
			log.Errorf("Record fetch error - %v for UID - %v", err, updateReq.UID)
			if err == mongo.ErrNoDocuments {
				errList = append(errList, response.NewErrorDetail(http.StatusNotFound, fmt.Sprintf("UID - [%v] Update error - [%v]", updateReq.UID, constants.RECORD_NOT_FOUND_ERROR)))
			} else {
				errList = append(errList, response.NewErrorDetail(http.StatusBadRequest, fmt.Sprintf("UID - [%v] Update error - [%v]", updateReq.UID, err)))
			}
		} else {
			// indivisual user role update
			user.Role = updateReq.ROLES
			userList = append(userList, *user)
		}
	}
	log.Info("Exiting validate.ValidateAndFormUpdateRoleUserObject() Method")
	return
}

func checkIfExistsInList(userList []models.Users, UID string) bool {
	flag := false
	for _, user := range userList {
		if user.UID == UID {
			flag = true
		}
	}
	return flag
}
