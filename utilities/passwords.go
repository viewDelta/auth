package utilities

import (
	"golang.org/x/crypto/bcrypt"
)

//HashPassword used to create password hash
func HashPassword(password string, hashingStrength int) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), hashingStrength)
	return string(bytes), err
}

//CheckPasswordHash validation for provided password with password hash
//Returns true -> if both are same
// 		  false -> if both are different
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
