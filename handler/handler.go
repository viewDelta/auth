package handler

import "github.com/labstack/echo/v4"

//AuthInterface interface for it's handlers
type AuthInterface interface {
	Login(c echo.Context) error
	SignUp(c echo.Context) error
	VerifyAccount(c echo.Context) error
	ResetPassword(c echo.Context) error
	FetchNonAdminUsers(c echo.Context) error
	ActivateUser(c echo.Context) error
	RemoveUser(c echo.Context) error
	UpdateUserRole(c echo.Context) error
}
