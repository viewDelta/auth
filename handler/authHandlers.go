package handler

import (
	"net/http"
	"time"

	"bitbucket.org/viewDelta/auth/utilities"
	helper "bitbucket.org/viewDelta/service-core/app-helper"
	"bitbucket.org/viewDelta/service-core/intercept"
	_ "bitbucket.org/viewDelta/service-core/locales"
	"bitbucket.org/viewDelta/service-core/outbound"
	"bitbucket.org/viewDelta/service-core/repository"
	"bitbucket.org/viewDelta/service-core/repository/users"
	"bitbucket.org/viewDelta/service-core/utils"

	Message "bitbucket.org/viewDelta/service-core/constants"
	Settings "bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/headers"
	"bitbucket.org/viewDelta/service-core/locales/request"
	"bitbucket.org/viewDelta/service-core/locales/response"
	models "bitbucket.org/viewDelta/service-core/models"
	"github.com/dennisstritzke/httpheader"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"github.com/rs/xid"
)

var (
	userDAO    repository.UserDAO
	validator  utilities.IAuthValidator
)

func init() {
	userDAO = users.New()
	validator = utilities.GetValidator()
}

type handlers struct{}

//New for creating new package object
func New() AuthInterface {
	return &handlers{}
}

// Login method will create login token
// @Summary Login into application
// @Description Login into application with email, password and cache request
// @Tags auth
// @Accept json
// @Produce json
// @Param login body request.LoginReq true "Login Request payload"
// @Success 200 {object} response.LoginRes "Logged In Success"
// @Failure 400 {object} locales.ResponseTemplate "Missing Body or Invalid Payload"
// @Failure 401 {object} locales.ResponseTemplate "User isn't activated or verified"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /login [post]
func (handle *handlers) Login(c echo.Context) error {
	var (
		reqBody request.LoginReq
		resBody response.LoginRes
	)
	if err := c.Bind(&reqBody); err != nil {
		return helper.SendGenericResponse(c, http.StatusBadRequest, Message.INVALID_PAYLOAD)
	}
	user, err := validator.ValidateSignInReqPayload(&reqBody)
	if err != nil {
		log.Errorf("Login Request Body Validation Error [%v]", err)
		return helper.SendGenericResponse(c, http.StatusBadRequest, err.Error())
	}
	if user.Verified != true || user.Active != true {
		return helper.SendGenericResponse(c, http.StatusUnauthorized, Message.USER_ACCOUNT_NOT_ACTIVE_OR_VERIFIED)
	}
	if user.Session.Id == "" {
		tokenDetails, err := utils.GetToken(user, Settings.CLIENT)
		if err != nil {
			log.Errorf("Token Generation Error [%v]", err)
			return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.TOKEN_GENERATION_ERROR)
		}
		user.Session.Id = tokenDetails.TokenID
		user.Session.ReqAt = tokenDetails.ReqAt
		user.Session.ExpAt = tokenDetails.ExpAt
		if err := userDAO.UpdateUser(user); err != nil {
			log.Errorf("User Record Couldn't be updated [%v]", err)
			return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.APPLICATION_ERROR)
		}
		//Success Response body formation
		resBody.Data.Token = tokenDetails.TokenID
		resBody.Data.CreatedAt = tokenDetails.ReqAt
		resBody.Data.ExpAt = tokenDetails.ExpAt
	} else {
		expTime := user.Session.ExpAt
		if err != nil {
			log.Errorf("Time Conversion Error [%v]", err)
			return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.APPLICATION_ERROR)
		}
		if int(expTime.Sub(time.Now()).Hours()) <= Settings.TokenRenewTime {
			tokenDetails, err := utils.RenewToken(user.Session.Id)
			if err != nil {
				log.Errorf("Token Renew Error [%v]", err)
				return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.APPLICATION_ERROR)
			}
			user.Session.Id = tokenDetails.TokenID
			user.Session.ReqAt = tokenDetails.ReqAt
			user.Session.ExpAt = tokenDetails.ExpAt
			if err := userDAO.UpdateUser(user); err != nil {
				log.Errorf("User Record Couldn't be updated [%v]", err)
				return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.APPLICATION_ERROR)
			}
			//Fill response in token
			//Success Response body formation
			resBody.Data.Token = tokenDetails.TokenID
			resBody.Data.CreatedAt = tokenDetails.ReqAt
			resBody.Data.ExpAt = tokenDetails.ExpAt
		} else {
			//Success Response body formation
			resBody.Data.Token = user.Session.Id
			resBody.Data.CreatedAt = user.Session.ReqAt
			resBody.Data.ExpAt = user.Session.ExpAt
		}
		//Setting up cookie in response header
		cookie := new(http.Cookie)
		cookie.Name = Settings.COOKIE_TOKEN_NAME
		cookie.Value = resBody.Data.Token
		if reqBody.Persist {
			cookie.Expires = time.Now().AddDate(0, 1, 0)
		}
		cookie.Path = "/"
		if c.Request().Header.Get(Settings.X_Forwarded_Host) != "" {
			cookie.Domain = c.Request().Header.Get(Settings.X_Forwarded_Host)
		} else {
			cookie.Domain = Settings.DEFAULT_HOST
		}
		c.SetCookie(cookie)
	}
	//Forming response status code & message
	resBody.Status.Code = http.StatusOK
	resBody.Status.Msg = Message.LOGGED_IN_SUCESS
	return c.JSON(http.StatusOK, &resBody)
}

// SignUp method will create account and will trigger mailing request
// @Summary SignUp to create account
// @Description SignUp method will create account and will trigger mailing request
// @Tags auth
// @Accept json
// @Produce json
// @Param signup body request.SignUpReq true "Signup Request payload"
// @Success 201 {object} locales.ResponseTemplate "account has been created"
// @Success 202 {object} locales.ResponseTemplate "account has been created but unable to send signup mail"
// @Failure 400 {object} locales.ResponseTemplate "Missing Body or Invalid Payload"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /signup [post]
func (handle *handlers) SignUp(c echo.Context) error {
	var (
		user    models.Users
		reqBody request.SignUpReq
	)
	if err := c.Bind(&reqBody); err != nil {
		return helper.SendGenericResponse(c, http.StatusBadRequest, Message.INVALID_PAYLOAD)
	}
	if err := validator.ValidateSignUpReqPayload(&reqBody); err != nil {
		log.Errorf("Request Sign up validation error [%v]", err)
		return helper.SendGenericResponse(c, http.StatusBadRequest, err.Error())
	}

	//user value update
	user.UID = xid.New().String()
	user.FirstName = reqBody.FirstName
	user.LastName = reqBody.LastName
	user.Email = reqBody.Email
	user.Role = reqBody.RoleID
	if hashPwd, err := utilities.HashPassword(reqBody.Password, Settings.PasswordHash); err != nil {
		log.Errorf("Password Hashing Error - [%v]", err)
		return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.PASSWORD_HASH_ERROR)
	} else {
		user.Password = hashPwd
	}
	user.Active = false
	user.Verified = false
	if err := userDAO.InsertUser(&user); err != nil {
		log.Errorf("Record Insertion Error Details [%v]", err)
		return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.APPLICATION_ERROR)
	}
	//Mailing Service call
	if err := outbound.MailCall(headers.GetSignUpHeader(), user.Email, c.Request().Header.Get(Settings.X_Forwarded_Host)); err != nil {
		log.Errorf("Error Encountered When Calling For Mail Service [%v]", err)
		return helper.SendGenericResponse(c, http.StatusAccepted, Message.SIGNUP_SUCESS_WITHOUT_VERIFY_MAIL)
	}
	return helper.SendGenericResponse(c, http.StatusCreated, Message.SIGNUP_SUCESS)
}

// VerifyAccount will verify user account will be triggered post mail click
// @Summary Account Verification Update
// @Description VerifyAccount will verify user account will be triggered post mail click
// @Tags auth
// @Accept json
// @Produce json
// @Param Token header string true "login token"
// @Success 200 {object} locales.ResponseTemplate "user account activated"
// @Failure 400 {object} locales.ResponseTemplate "Missing or Invalid header"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /verify [put]
func (handle *handlers) VerifyAccount(c echo.Context) error {
	var (
		reqHeaders headers.TokenHeader
	)
	//Header validation --> Token [SignUp Verify Token]
	if err := httpheader.Bind(c.Request().Header, &reqHeaders); err != nil {
		log.Errorf("Header parsing error [%v]", err)
		return helper.SendGenericResponse(c, http.StatusBadRequest, Message.INVALID_HEADER)
	}
	//Setting Token Header as Verify Account
	reqHeaders.SetVerifyAccount()
	//Token details fetch from token struct
	user, err := reqHeaders.ValidateToken(userDAO)
	if err != nil {
		log.Errorf("Token Process Error [%v]", err)
		return helper.SendGenericResponse(c, http.StatusBadRequest, err.Error())
	}
	//Setting up the property for activation
	user.Verified = true
	user.VerifyRequest.EnabledAt = time.Now()
	if err := userDAO.UpdateUser(user); err != nil {
		log.Errorf("User Updation Error [%v]", err)
		return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.APPLICATION_ERROR)
	}
	return helper.SendGenericResponse(c, http.StatusOK, Message.USER_ACTIVATED)
}

// ResetPassword will reset the password for the given user will be triggered post mail click
// @Summary Restoring Password
// @Description ResetPassword will reset the password for the given user will be triggered post mail click
// @Tags auth
// @Accept json
// @Produce json
// @Param Token header string true "reset or session token"
// @Param reset body request.ResetPasswordRequest true "reset payload"
// @Success 200 {object} locales.ResponseTemplate "password update successfully"
// @Failure 400 {object} locales.ResponseTemplate "Either header or payload is Missing or Invalid"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /reset [put]
func (handle *handlers) ResetPassword(c echo.Context) error {
	var (
		reqHeaders headers.TokenHeader
		reqBody    request.ResetPasswordRequest
	)
	//Header validation --> Token [Reset Password Token]
	if err := httpheader.Bind(c.Request().Header, &reqHeaders); err != nil {
		log.Errorf("Header parsing error [%v]", err)
		return helper.SendGenericResponse(c, http.StatusBadRequest, Message.INVALID_HEADER)
	}
	if err := c.Bind(&reqBody); err != nil {
		return helper.SendGenericResponse(c, http.StatusBadRequest, Message.INVALID_PAYLOAD)
	}
	if err := validator.ValidatePassword(reqBody.Password); err != nil {
		log.Errorf("Invalid Password provided [%v]", err)
		return helper.SendGenericResponse(c, http.StatusBadRequest, Message.INVALID_PASSWORD)
	}
	//Setting Request type header as setForgotpassword type
	reqHeaders.SetForgotPwdOrSession()
	//Token details fetch from token struct
	user, err := reqHeaders.ValidateToken(userDAO)
	if err != nil {
		log.Errorf("User Data fetch error [%v]", err)
		return helper.SendGenericResponse(c, http.StatusBadRequest, err.Error())
	}
	//Checks if password change is same
	if utilities.CheckPasswordHash(reqBody.Password, user.Password) {
		log.Infof("No password change requested")
		return helper.SendGenericResponse(c, http.StatusBadRequest, Message.NO_CHANGE_REQUESTED)
	}
	if user.Password, err = utilities.HashPassword(reqBody.Password, Settings.PasswordHash); err != nil {
		log.Errorf("Password Hashing Error - [%v]", err)
		return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.PASSWORD_HASH_ERROR)
	}
	if err = userDAO.UpdateUser(user); err != nil {
		log.Errorf("User Update Error [%v]", err)
		return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.APPLICATION_ERROR)
	}
	return helper.SendGenericResponse(c, http.StatusOK, Message.PASSWORD_UPDATE_SUCCESS)
}

// Fetch Non-Admin Users will return list of users
// @Summary Fetch Non-Admin Users
// @Description Fetch Non-Admin Users will return list of users
// @Tags auth
// @Accept json
// @Produce json
// @Param Token header string true "session token"
// @Success 200 {object} locales.ResponseTemplate "record fetched successfully"
// @Failure 401 {object} locales.ResponseTemplate "Invalid Token"
// @Failure 400 {object} locales.ResponseTemplate "Either header is Missing or Invalid"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /getUsers [get]
func (handle *handlers) FetchNonAdminUsers(c echo.Context) error {
	var apiResponse response.NonAdminListResponse
	if err:= intercept.SecurityFilter(c); err != nil {
		return helper.SendGenericResponse(c, http.StatusUnauthorized, err.Error())
	}
	allNonAdmin, err := userDAO.FetchAllNonAdmin()
	if err != nil {
		log.Errorf("Error While fetching all records [%v]", err)
		return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.APPLICATION_ERROR)
	}
	apiResponse.Code = http.StatusOK
	apiResponse.Msg = Message.RECORD_FETCHED_SUCCESS
	for _, v := range allNonAdmin {
		var tempUserData response.UserDetails
		tempUserData.ID = v.UID
		tempUserData.FirstName = v.FirstName
		tempUserData.LastName = v.LastName
		tempUserData.Activated = v.Active
		tempUserData.Verified = v.Verified
		apiResponse.UserDetailsList = append(apiResponse.UserDetailsList, tempUserData)
	}
	return c.JSON(http.StatusOK, &apiResponse)
}

// ActivateUser will activate the user Activate boolean flag
// @Summary Update the user Activate flag
// @Description ActivateUser will activate the user Activate boolean flag
// @Tags auth
// @Accept json
// @Produce json
// @Param Token header string true "session token"
// @Param activateUser body request.ActivateReq true "activate user body"
// @Success 200 {object} locales.ResponseTemplate "record updated successfully"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 400 {object} locales.ResponseTemplate "Either header is Missing or Invalid or no change has been requested"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /user/{uid} [put]
func (handle *handlers) ActivateUser(c echo.Context) error {
	var reqBody  request.ActivateReq
	if err:= intercept.SecurityFilter(c); err != nil {
		return helper.SendGenericResponse(c, http.StatusUnauthorized, err.Error())
	}
	if err := c.Bind(&reqBody); err != nil{
		return helper.SendGenericResponse(c, http.StatusBadRequest, err.Error())
	}
	// User ID from path `users/:id`
	uid := c.Param("uid")
	user, err := intercept.NonAdminValidator(uid)
	if err != nil {
		return helper.SendGenericResponse(c, http.StatusBadRequest, err.Error())
	} else if user.Active == reqBody.Flag {
		log.Infof("No Active flag change request received")
		return helper.SendGenericResponse(c, http.StatusBadRequest, Message.NO_CHANGE_REQUESTED)
	}
	user.Active = reqBody.Flag //Setting request body flag
	if err := userDAO.UpdateUser(user); err != nil {
		log.Errorf("Record Update error [%v]", err)
		return helper.SendGenericResponse(c, http.StatusInternalServerError, Message.APPLICATION_ERROR)
	}
	return helper.SendGenericResponse(c, http.StatusOK, Message.RECORD_UPDATE_SUCCESS)
}

// RemoveUser will remove user from db
// @Summary RemoveUser will remove user
// @Description RemoveUser will remove user from db
// @Tags auth
// @Accept json
// @Produce json
// @Param Token header string true "session token"
// @Success 200 {object} locales.ResponseTemplate "record deleted successfully"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 400 {object} locales.ResponseTemplate "Either header is Missing or Invalid"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /user/{uid} [delete]
func (handle *handlers) RemoveUser(c echo.Context) error {
	if err := intercept.SecurityFilter(c); err != nil {
		return helper.SendGenericResponse(c, http.StatusUnauthorized, err.Error())
	}
	// User ID from path `users/:id`
	uid := c.Param("uid")
	user, err := intercept.NonAdminValidator(uid)
	if err != nil {
		return helper.SendGenericResponse(c, http.StatusBadRequest, err.Error())
	}
	if err := userDAO.DropUser(user.Email); err != nil {
		log.Errorf("Record Remove error [%v]", err)
		return helper.ErrorHandler(c)
	}
	return helper.SendGenericResponse(c, http.StatusOK, Message.RECORD_DELETED)
}

// Updates Users Role In Database
// @Summary Updates Users Role In Database
// @Description Updates Users Role In Database
// @Tags auth
// @Accept json
// @Produce json
// @Param Token header string true "session token"
// @Param UpdateRole body []request.UpdateRolesRequest true "Update Role ID for user"
// @Success 200 {object} locales.ResponseTemplate "records has been updated"
// @Success 202 {object} response.UpdateResponse "record has been processed partially"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 400 {object} response.UpdateResponse "Either header is Missing or Invalid or request body contains some error"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /users [put]
func (handle *handlers) UpdateUserRole(c echo.Context) error {
	log.Info("Entering authHandler.UpdateUserRole() Method")
	var reqBody    []request.UpdateRolesRequest
	//Access Check for user
	if err := intercept.SecurityFilter(c); err != nil {
		return helper.SendGenericResponse(c, http.StatusUnauthorized, err.Error())
	}
	if err := c.Bind(&reqBody); err != nil {
		log.Errorf("Invalid Payload Received [%v]", err)
		return helper.SendGenericResponse(c, http.StatusBadRequest, Message.INVALID_PAYLOAD)
	}
	// ----------- Payload validation -----------
	userList, errList := validator.ValidateAndFormUpdateRoleUserObject(reqBody)
	if len(errList) == len(reqBody) {
		return c.JSON(http.StatusBadRequest, response.FormUpdateResponse(http.StatusBadRequest, Message.REQUEST_PROCESSING_ERROR, errList))
	} else if len(errList) != 0 {
		log.Errorf("Invalid payload details - %v", errList)
		// process valid data
		processRoleUpdate(userList)
		return c.JSON(http.StatusAccepted, response.FormUpdateResponse(http.StatusAccepted, Message.RECORD_PROCESSED_PARTIALLY, errList))
	}
	//process the entire data
	processRoleUpdate(userList)
	log.Info("Exiting authHandler.UpdateUserRole() Method")
	return helper.SendGenericResponse(c, http.StatusOK, Message.RECORD_PROCESSED_SUCCESS)
}

// processRoleUpdate will update
func processRoleUpdate(userList []models.Users) {
	for _, user := range userList {
		if err := userDAO.UpdateUser(&user); err != nil {
			log.Errorf("User Role Updation Failed - [%v]", err)
		}
	}
}
