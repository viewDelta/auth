module bitbucket.org/viewDelta/auth

go 1.14

require (
	bitbucket.org/viewDelta/service-core v1.1.0
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/dennisstritzke/httpheader v0.0.0-20191001125049-c4997e500c10
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/spec v0.20.3 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/labstack/echo/v4 v4.1.16
	github.com/labstack/gommon v0.3.0
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/rs/xid v1.2.1
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/swaggo/echo-swagger v1.0.0
	github.com/swaggo/swag v1.6.7
	go.mongodb.org/mongo-driver v1.3.4
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.5 // indirect
)

replace bitbucket.org/viewDelta/service-core => ../service-core
